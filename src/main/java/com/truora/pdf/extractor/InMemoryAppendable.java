/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truora.pdf.extractor;

import java.io.IOException;

/**
 *
 * @author luis
 */
class InMemoryAppendable implements Appendable {

    private String vals;

    public InMemoryAppendable() {
        this.vals = "";
    }

    public String getVals() {
        return vals;
    }

    @Override
    public Appendable append(char el) {
        this.vals = this.vals + el;
        return this;
    }

    @Override
    public Appendable append(CharSequence cs) {
        this.vals = this.vals + cs;
        return this;
    }

    @Override
    public Appendable append(CharSequence cs, int i, int i1) throws IOException {
        this.vals = this.vals + cs;
        return this;
    }
}
