/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truora.pdf.extractor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Base64;
import org.apache.pdfbox.pdmodel.PDDocument;

import technology.tabula.PageIterator;
import technology.tabula.Page;
import technology.tabula.Table;
import technology.tabula.ObjectExtractor;
import technology.tabula.extractors.SpreadsheetExtractionAlgorithm;
import technology.tabula.writers.CSVWriter;
import technology.tabula.writers.Writer;

/**
 *
 * @author luis
 */
public class Main {
    
    public static void main(String[] args) throws IOException {
        Main main = new Main();
        File pdfDocument = new File("postulados.pdf");
        byte[] bytes = Util.loadFile(pdfDocument);
        byte[] encoded = Base64.getEncoder().encode(bytes);
        String encodedString = new String(encoded);
        String output = main.pdfToCsv(encodedString);
        System.out.println(output);
    }
    /**
     * Convert a encoded PDF in base64 to string in CSV format
     * @param input PDF encoded in base64
     * @return string in CSV format
     * @throws IOException 
     */
    public String pdfToCsv(String input) throws IOException {
        byte[] bytes = Util.decodeBase64(input);
        PDDocument pddDocument = PDDocument.load(bytes);
        SpreadsheetExtractionAlgorithm spreadsheetExtractor = new SpreadsheetExtractionAlgorithm();
        PageIterator pageIterator = getPageIterator(pddDocument);
        List<Table> tables = new ArrayList<>();

        while (pageIterator.hasNext()) {
            Page page = pageIterator.next();
            tables.addAll(spreadsheetExtractor.extract(page));
        }
        InMemoryAppendable apdb = new InMemoryAppendable();
        Writer writer = new CSVWriter();
        writer.write(apdb, tables);
        return apdb.getVals();
    }

    private PageIterator getPageIterator(PDDocument pdfDocument) throws IOException {
        ObjectExtractor extractor = new ObjectExtractor(pdfDocument);
        return extractor.extract();
    }

}
